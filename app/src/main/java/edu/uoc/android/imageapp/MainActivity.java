package edu.uoc.android.imageapp;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener
{

    // Codigo aleatoreos para la petición de permisos en tiempo de ejecución
    private final int REQUEST_PERMISSION_STORAGE_SAVE = 101;
    private final int REQUEST_PERMISSION_STORAGE_DELETE = 102;
    //Valor de la constante para el acceso a la cámara del sistema operativo
    static final int REQUEST_IMAGE_CAPTURE = 1;
    //Instancia de Bitmap para la creación de la imagen en memoria
    Bitmap imageBitmap;
    //Constantes para almacenar los nombres carpetas y archivo (foto)
    private final String FOLDER_NAME = "UOCImageApp";
    private final String FILE_NAME = "imageapp.jpg";

    // Views
    private Button buttonOpenImage;
    private ImageView imageView;
    private TextView textViewMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Set views
        buttonOpenImage = findViewById(R.id.buttonCapture);
        imageView = findViewById(R.id.image_app_iv_picture);
        textViewMessage = findViewById(R.id.image_app_tv_message);
        // Set listeners
        buttonOpenImage.setOnClickListener(this);


        //Carga una imagen en la pantalla principal dependiendo si existe en el almacenamiento
        cargarImagenSiExiste();
    }

    //Método sobre escrito que asigna el archivo de menú para la activity actual
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        Log.i("AM1", "Menú");
        Log.d("debug", "Debug");

        return true;

    }

    //Método sobre escrito que realiza una acción dependiendo del menú seleccionado
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_delete) {
            onDeleteMenuTap();
            return true;
        } else
            if (item.getItemId() == R.id.action_save) {
            onSaveMenuTap();
            return true;
        }
            else
                if(item.getItemId() == R.id.btn_salir){
                    CerrarAplicacion();
                    return true;
                }
        return super.onOptionsItemSelected(item);
    }

    //Función para cerrar la app
    private void CerrarAplicacion(){
        finish();
    }

    private void onDeleteMenuTap() {
        // revisa permisos de eliminación de archivos en el almacenamiento
        if (!hasPermissionsToWrite()) {
            // en el caso no tener permisos los solicita en tiempo de ejecución
            ActivityCompat.requestPermissions(
                    this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                              REQUEST_PERMISSION_STORAGE_DELETE);
        } else {
            //En el caso que si tenga permisos ejecuta una
            //ventana de diálogo con DialogInterface para preguntarle
            //al usuario si está seguro de eliminar la imagen
                DialogInterface.OnClickListener dialogClickListener =
                        new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                try {
                                    deleteImageFile();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };
                //Diseñamos la pantalla con la pregunta y las opciones con AlertDialog
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Eliminar Imagen")
                        .setMessage("Desea eliminar esta imagen?")
                        .setPositiveButton("Si", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener)
                        .show();
        }
    }

    private boolean hasPermissionsToWrite() {
        return ContextCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void onSaveMenuTap() {
        if (!hasPermissionsToWrite()) {
            ActivityCompat.requestPermissions(
                    this, new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION_STORAGE_SAVE);
        } else {
            //en el caso de tener permiso procede a crear una carpeta y a guardar el archivo
            //de imagen en el almacenamiento del dispositivo
            if (imageView.getDrawable() != null) {
                //Create a folder named UOCImageApp
                createFolder();
                //save Directory
                String storageDir =
                        Environment.getExternalStorageDirectory() + "/UOCImageApp/";
                //Save the photo in the specified dir
                createImageFile(storageDir, this.FILE_NAME, imageBitmap);
            } else {
                Toast.makeText(this, "Tome una foto primero!",
                        Toast.LENGTH_LONG).show();
            }
        }
    }




    String mCurrentPhotoPath;

    //Método que crea el archivo de imagen
    private void createImageFile(
            String storageDir, String fileName, Bitmap bitmap) {

        try {

            File myFile = new File(storageDir, fileName);
            FileOutputStream stream = new FileOutputStream(myFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            stream.flush();
            stream.close();
            Toast.makeText(this,
                    "Imagen guardada!", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //Método que elimina el archivo de imagen de una dirección predeterminada
    private void deleteImageFile() throws IOException {

        File storageDir = new File(
                Environment.getExternalStorageDirectory() + "/UOCImageApp");
        File image = new File(storageDir + "/" + this.FILE_NAME);

        //Si existe el archivo entonces lo elimina, caso contrario
        //envía un mensaje de que no se encuentra el archivo
        if (image.exists()) {
            image.delete();
            Toast.makeText(this, "File deleted!", Toast.LENGTH_LONG).show();
            imageView.setImageResource(0);
            textViewMessage.setVisibility(View.VISIBLE);
        }
        else {
            Toast.makeText(this, "File not found!", Toast.LENGTH_LONG).show();

        }
    }

    //Procedimiento que carga la imagen desde la SDCard si es que existe, caso contrario
    // muestra el elemento TextView con el mensaje
    private void cargarImagenSiExiste()
    {
        //OBTENEMOS LA DIRECCION DE LA IMAGEN
        String myImage = Environment.getExternalStorageDirectory()
                .getAbsolutePath()+"/" + FOLDER_NAME + "/" + FILE_NAME;
        //OBTENEMOS LA IMAGEN
        File imagen =new File(myImage);
        if(imagen.exists()){
            //LEER LA IMAGEN DE LA SDCARD UTILIZANDO PICASSO LIBRARY
            //https://square.github.io/picasso/
            Picasso.get().load(imagen).into(imageView);
            textViewMessage.setVisibility(View.INVISIBLE);
        }
        else {
            textViewMessage.setVisibility(View.VISIBLE);
        }
    }

    //Procedimiento que se encarga de la creación de la carpeta con el nombre UOCImageApp
    private void createFolder(){

        String myfolder = Environment.getExternalStorageDirectory()
                .getAbsolutePath()+"/"+ FOLDER_NAME;

        File folder =new File(myfolder);
        if(!folder.exists())
            if(!folder.mkdir()){
                Toast.makeText(this,
                        FOLDER_NAME + " no se puede crear ",
                        Toast.LENGTH_LONG).show();
            }
            else
                Toast.makeText(this,
                        FOLDER_NAME + " creada exitosamente",
                        Toast.LENGTH_LONG).show();
    }



    @Override
    public void onClick(View v) {
        if (v == buttonOpenImage) {
            TakePictureIntent();
        }
    }

    //Intent que ejecuta la pantalla de la cámara del dispositivo
    private void TakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }
    //Despues que la activity de la cámara toma la foto y el usuario acepta la foto
    //esta se asigna al ImageView para ser mostrado, tambien se oculta el TextView
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            //Obtenemos de data la imagen de la cámara
            Bundle extras = data.getExtras();
            imageBitmap = (Bitmap) extras.get("data");
            //Asiganmos la iamgen al ImageView
            imageView.setImageBitmap(imageBitmap);
            //Ocultamos el TextView
            textViewMessage.setVisibility(View.INVISIBLE);
        }
    }

    //Se gestiona los resultados de la solicitud de los permisos en tiempo de ejecución
    @Override
    public void onRequestPermissionsResult(
            int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_STORAGE_DELETE: {
                if (grantResults.length > 0 && grantResults[0]
                        == PackageManager.PERMISSION_GRANTED) {
                } else {
                    Toast.makeText(this,
                            "Permiso de eliminación denegado", Toast.LENGTH_LONG).show();
                }
            }
            case REQUEST_PERMISSION_STORAGE_SAVE: {
                if (grantResults.length > 0 && grantResults[0]
                        == PackageManager.PERMISSION_GRANTED) {
                    createFolder();
                    String storageDir =
                            Environment.getExternalStorageDirectory()+"/UOCImageApp/";
                    createImageFile(storageDir, FILE_NAME, imageBitmap);
                } else {
                    Toast.makeText(this,
                            "Permiso de guardar denegado", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
